package main

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
)

var (
	//go:embed sql/add.sql
	addSQL string

	//go:embed sql/last.sql
	lastSQL string
)

// zero vs missing value
type Entry struct {
	Time    time.Time `json:"time"`
	User    string    `json:"login"`
	Content string    `json:"content"`
	// APIToken string    `json:"-"`
}

type ValidationError struct {
	Field  string
	Reason string
}

func (e *ValidationError) Error() string {
	return fmt.Sprintf("%s: %s (host: localhost)", e.Field, e.Reason)

}

func (e *Entry) Validate() error {
	if e.User == "" {
		return &ValidationError{"User", "missing"}
	}

	if e.Content == "" {
		return &ValidationError{"Content", "missing"}
	}

	return nil
}

type DB struct {
	db *sql.DB
}

func NewDB(ctx context.Context, dsn string) (*DB, error) {
	conn, err := sql.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}

	db := DB{conn}
	if err := db.Health(ctx); err != nil {
		conn.Close()
		return nil, err
	}

	return &db, nil
}

// Exercise: Change the database methods to use context. e.g. db.Exec -> db.ExecContext
// Context is passed to database methods, use r.Context() in the HTTP handler

func (d *DB) Add(ctx context.Context, entry Entry) error {
	_, err := d.db.ExecContext(ctx, addSQL, entry.Time, entry.User, entry.Content)
	return err
}

func (d *DB) Last(ctx context.Context) (Entry, error) {
	row := d.db.QueryRowContext(ctx, lastSQL)
	var e Entry
	if err := row.Scan(&e.Time, &e.User, &e.Content); err != nil {
		return Entry{}, err
	}

	return e, nil
}

func (d *DB) Health(ctx context.Context) error {
	return d.db.PingContext(ctx)
}

func (d *DB) Close() error {
	return d.db.Close()
}
