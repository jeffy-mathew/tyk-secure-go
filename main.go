package main

import (
	"bytes"
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	"golang.org/x/time/rate"
)

var (
	//go:embed html/last.html
	lastHTML string

	limiter = rate.NewLimiter(rate.Every(time.Minute), 100)
)

const (
	maxRequestSize = 1_000_000 // MB
)

type Server struct {
	db   *DB
	auth *Auth
}

// Exercise: Fix XSS either using html/template or html.EscapeString
func (s *Server) lastHandler(w http.ResponseWriter, r *http.Request) {
	lastText := "No entries"

	entry, err := s.db.Last(r.Context())
	if err == nil {
		time := entry.Time.Format("2006-01-02T15:04")
		lastText = fmt.Sprintf("[%s] %s by %s", time, entry.Content, entry.User)
	}
	lastText = html.EscapeString(lastText)
	fmt.Fprintf(w, lastHTML, lastText)
}

// "<" -> "&lt;"

// exercise: Use basic auth to authenticate the user
// login, passwd, ok := r.BasicAuth()
func (s *Server) newHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Utility functions for context & params
	params, ok := r.Context().Value(ctxKey).(*ctxParams)
	if !ok || params == nil {
		http.Error(w, "missing parameters", http.StatusInternalServerError)
		return
	}

	if params.user.Login == "" {
		http.Error(w, "missing auth", http.StatusUnauthorized)
		return

	}

	defer r.Body.Close()

	rdr := http.MaxBytesReader(w, r.Body, maxRequestSize)
	defer rdr.Close()
	var e Entry

	// Save raw data for debug
	var buf bytes.Buffer
	tr := io.TeeReader(rdr, &buf)

	if err := json.NewDecoder(tr).Decode(&e); err != nil {
		http.Error(w, fmt.Sprintf("[%s] bad json: %s", params.id, err.Error()), http.StatusBadRequest)
		fmt.Printf("data: %q\n", buf.String())
		return
	}

	if err := e.Validate(); err != nil {
		// type assertion (the old way)
		// if ve, ok := err.(*ValidationError); ok {
		var ve *ValidationError
		msg := fmt.Sprintf("[%s] bad entry", params.id)
		if errors.As(err, &ve) {
			msg = fmt.Sprintf("%s: %s - %s", msg, ve.Field, ve.Reason)
		}

		http.Error(w, msg, http.StatusBadRequest)
		fmt.Printf("data: %q\n", buf.String())
		return
	}

	e.Time = time.Now()
	if err := s.db.Add(r.Context(), e); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(e)
}

func (s *Server) Health(ctx context.Context) error {
	return s.db.Health(ctx)
}

func (s *Server) healthHandler(w http.ResponseWriter, r *http.Request) {
	if !limiter.Allow() {
		status := http.StatusTooManyRequests
		http.Error(w, http.StatusText(status), status)
		return
	}

	if err := s.Health(r.Context()); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "OK\n")
}

type ctxKeyType struct{}

var ctxKey ctxKeyType

type ctxParams struct {
	user User
	id   string
}

func (s *Server) authMiddleware(h http.Handler) http.Handler {

	fn := func(w http.ResponseWriter, r *http.Request) {
		login, passwd, ok := r.BasicAuth()
		if !ok {
			http.Error(w, "missing auth", http.StatusUnauthorized)
			return
		}
		user, err := s.auth.Login(login, passwd)
		if err != nil {
			http.Error(w, "bad auth", http.StatusUnauthorized)
			return
		}
		// exercise: Add authorization here
		// r.URL.Path
		// r.Method

		// before
		params := ctxParams{
			user: user,
			id:   xid.New().String(),
		}
		ctx := context.WithValue(r.Context(), ctxKey, &params)
		r = r.WithContext(ctx)
		h.ServeHTTP(w, r)
		log.Printf("INFO: %s called", r.URL.Path)
		// after
	}

	return http.HandlerFunc(fn)
}

func main() {
	var err error
	db, err := NewDB(context.Background(), "host=localhost user=postgres password=s3cr3t sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	auth := NewAuth()
	// FIXME: Plain text passwd
	auth.AddUser("joe", "baz00ka", Writer)
	auth.AddRule(Rule{"/new", http.MethodPost, Writer})
	s := Server{db, auth}

	r := mux.NewRouter()
	r.HandleFunc("/last", s.lastHandler).Methods(http.MethodGet)
	// r.HandleFunc("/new", s.newHandler).Methods(http.MethodPost)
	h := s.authMiddleware(http.HandlerFunc(s.newHandler))
	r.Handle("/new", h).Methods(http.MethodPost)
	r.HandleFunc("/health", s.healthHandler).Methods(http.MethodGet)

	http.Handle("/", r)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
