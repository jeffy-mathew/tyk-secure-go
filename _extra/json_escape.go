package main

import (
	"encoding/json"
	"os"
)

func main() {
	json.NewEncoder(os.Stdout).Encode("<script>Pwnd!</script>")
	/*
		enc := json.NewEncoder(os.Stdout)
		enc.SetEscapeHTML(false)
		enc.Encode("<script>Pwnd!</script>")
	*/
}
