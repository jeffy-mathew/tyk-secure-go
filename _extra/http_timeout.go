package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	start := time.Now()
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancel()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8081/delay/2", nil)
	if err != nil {
		// Perl: die
		log.Fatalf("error: %s", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// Perl: die
		log.Fatalf("error: %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: bad status - %s", resp.Status)
	}
	fmt.Println("OK", time.Since(start))
}
