module gitlab.com/353workshops/tyk-secure-go

go 1.20

require (
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v5 v5.4.2
	github.com/rs/xid v1.5.0
	golang.org/x/time v0.3.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
