package main

import (
	"errors"
	"sync"
)

// For simplicity, never do IRL
var (
	ErrBadAuth = errors.New("bad authentication")
)

// bitmask
// Check: roles & role
// Combine: Reader|Writer
type Role byte

const (
	Reader Role = 1 << iota
	Writer
	Admin
)

func (r Role) HasRole(other Role) bool {
	return r&other != 0
}

type User struct {
	Login string
	Roles Role
}

type Rule struct {
	Path   string
	Method string
	Roles  Role
}

type Auth struct {
	mu    sync.Mutex
	db    map[string]User
	rules []Rule
}

func NewAuth() *Auth {
	a := Auth{
		db: make(map[string]User),
	}
	return &a
}

func (a *Auth) AddRule(r Rule) {
	a.rules = append(a.rules, r)
}

func (a *Auth) AddUser(login, passwd string, roles Role) {
	// FIXME: salt and hash passwd
	a.mu.Lock()
	defer a.mu.Unlock()

	a.db[passwd] = User{login, roles}
}

func (a *Auth) Login(login, passwd string) (User, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	u, ok := a.db[passwd]
	if !ok {
		return User{}, ErrBadAuth
	}

	if u.Login != login {
		return User{}, ErrBadAuth
	}

	return u, nil
}
